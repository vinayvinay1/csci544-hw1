1) part 1:

HAM :- Precision : 0.99
	Recall : 0.98
	F score : 0.986

SPAM :- Precision : 0.95
	Recall : 0.97
	F score : 0.962

POS :- Precision : 0.87
	Recall : 0.72
	F score : 0.79

NEG :- Precision : 0.76
	Recall : 0.89
	F score : 0.82



2) part 2:

SVM

HAM :- Precision : 0.99
	Recall : 0.99
	F score : 0.99

SPAM :- Precision : 0.97
	Recall : 0.98
	F score : 0.97

POS :- Precision : 0.89
	Recall : 0.85
	F score : 0.87

NEG :- Precision : 0.86
	Recall : 0.90
	F score : 0.88



MEGAM


HAM :- Precision : 0.86
	Recall : 0.41
	F score : 0.56

SPAM :- Precision : 0.34
	Recall : 0.82
	F score : 0.48

POS :- Precision : 0.88
	Recall : 0.84
	F score : 0.86

NEG :- Precision : 0.85
	Recall : 0.89
	F score : 0.87
