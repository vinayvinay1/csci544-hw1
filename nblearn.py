__author__ = 'vinaykumar'

import sys
import pickle

kvalue = 0
stotal = 0
kd = {}
classes = []
ncd = {}
pcd = {}
nid = {}
stopwords = {"the": "the", "and": "and", "a": "a", "of": "of", "to": "to", "is": "is", "in": "in", "that": "that",
            "I": "I", "it": "it", "this": "this", "/><br": "/><br", ".": ".", ",": ",", "-": "-", "_": "_", ":": ":",
             "/": "/"}

svm = {}


fin = open(sys.argv[1], 'r')
fout = open(sys.argv[2], 'wb')

for line in fin.read().splitlines():
    templine = line.split()
    stotal += 1

    uniquewords = set(templine[1:])

    for w in uniquewords:
        if w not in svm:
            svm.update({w: 1})

        else:
            svm[w] += 1

    if templine[0] not in pcd:
        pcd.update({templine[0]: 1})
        ncd.update({templine[0]: 0})
        nid.update({templine[0]: {}})
        classes.append(templine[0])

    else:
        pcd[templine[0]] += 1

    gen = (words for words in templine[1:] if words not in stopwords)
    for words in gen:

        # update individual class word count Nc
        ncd[templine[0]] += 1

        # update ni count for all words wrt each class
        if words not in nid[templine[0]]:
            nid[templine[0]].update({words: 2})

        else:
            nid[templine[0]][words] += 1

        # find value of K (total vocab count)
        if words not in kd:
            kvalue += 1
            kd.update({words: kvalue})

fin.close()

# Calculate P(C) dictionary for all classes
for l in iter(pcd):
    pcd[l] /= stotal

pickle.dump((pcd, ncd, nid, kvalue, classes, kd, svm), fout)

fout.close()



