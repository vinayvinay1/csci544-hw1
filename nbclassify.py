__author__ = 'vinaykumar'

import sys
import pickle
import math

fin = open(sys.argv[1], 'rb')
ftest = open(sys.argv[2], 'r', errors='ignore')
pcd, ncd, nid, kvalue, classes, kd, svm = pickle.load(fin)
fin.close()
pcmd = {}

for x in classes:
    pcmd.update({x: 0})

for line in ftest.read().splitlines():
    templine = line.split()

    for cls in classes:
        b = 0

        for words in templine:
            if words in nid[cls]:
                b += math.log(nid[cls][words]) - math.log(ncd[cls] + kvalue)

            else:
                b -= math.log(ncd[cls] + kvalue)

        #calculate P(C) of each sample
        pcmd[cls] = math.log(pcd[cls]) + b

    print(max(pcmd, key=pcmd.get))

ftest.close()